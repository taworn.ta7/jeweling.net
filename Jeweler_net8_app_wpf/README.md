# Jeweler_net8_app_wpf

Jeweler code for .NET 8 WPF.


## Features

* Logging with colorize
* Run installed, just copy files, or run portable


## Running

If you just run the App, you get normal mode.  In this mode, you get these folders:

* <App folder>/*.exe,dll
* <My Documents>/Jeweler_net8_app_wpf

If you create file 'portable', just blank file and no extension, in same exe folder.  You will get these folders:

* <App folder>/portable
* <App folder>/*.exe,dll
* <App folder>/Data

But, if you layout folders like these:

* <any folder>/App/*.exe,dll
* <any folder>/Data/  these folder must be created

You will get portable, too.


## Credits

Thank you, [Google Fonts](https://fonts.google.com/icons) API.

Thank you, [Melvin ilham Oktaviansyah](https://freeicons.io/profile/8939) on [freeicons.io](https://freeicons.io) for icons.

Thank you, [icon king1](https://freeicons.io/profile/3) on [freeicons.io](https://freeicons.io) for icons.


## Last

Sorry, but I'm not good at English. T_T

