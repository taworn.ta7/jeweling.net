﻿using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;
using Microsoft.Extensions.Logging;

namespace Jeweler_net8_app_wpf;

/// <summary>
/// Interaction logic for MainWindow.xaml
/// </summary>
public partial class MainWindow : Window
{
    private static readonly ILogger logger = AppBegin.Instance.Logging!.CreateLogger<MainWindow>();

    public MainWindow()
    {
        InitializeComponent();

        Loaded += (sender, e) =>
        {
            logger.LogTrace("I'm tracing.");
            logger.LogDebug("I'm debugging.");
            logger.LogInformation("I'm informing.");
            logger.LogWarning("I'm warning.");
            logger.LogError("I'm error!");
            logger.LogCritical("I'm critical!!");

            using (logger.BeginScope("Inner"))
            {
                logger.LogDebug("Dummy: {Dummy}", AppConfigs.Instance.Dummy);
            }
        };

        Unloaded += (sender, e) =>
        {
        };

        Closed += (sender, e) =>
        {
        };
    }

    // ----------------------------------------------------------------------

}
