﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Extensions.Logging;

namespace Jeweler_net8_app_wpf;

/// <summary>
/// Configuration as simple variables.
/// </summary>
public class AppConfigs
{
    private static readonly ILogger logger = AppBegin.Instance.Logging!.CreateLogger<AppConfigs>();

    //
    // Configuration
    //

    public string Dummy { get; private set; } = string.Empty;

    public static AppConfigs Instance
    {
        get => instance ??= new AppConfigs();
    }
    private static AppConfigs instance = null!;

    private AppConfigs()
    {
        Load();
    }

    // ----------------------------------------------------------------------

    public void Load()
    {
        var config = AppBegin.Instance.Config;
        var app = config.GetSection("App");
        Dummy = app?["Dummy"] ?? "-_-";
    }

    // ----------------------------------------------------------------------

    /// <summary>
    /// Output log for it's information.
    /// </summary>
    public void Print()
    {
        logger?.LogInformation("""
App Configs:

* Dummy: {Dummy}

""", Dummy);
    }
}
