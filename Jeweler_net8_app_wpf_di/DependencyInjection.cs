﻿using System;
using System.Windows;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Jeweler_net8_app_wpf_di.Extensions;

namespace Jeweler_net8_app_wpf_di;

/// <summary>
/// Dependency injection.
/// </summary>
public class DependencyInjection
{
    private static readonly ILogger logger = AppBegin.Instance.Logging!.CreateLogger<DependencyInjection>();

    public IServiceProvider Provider { get; private set; } = null!;

    public static DependencyInjection Instance
    {
        get => instance ??= new DependencyInjection();
    }
    private static DependencyInjection instance = null!;

    private DependencyInjection()
    {
        var collection = new ServiceCollection();

        // singleton(s)
        collection.AddSingleton(AppBegin.Instance.Config!);
        collection.AddSingleton(AppBegin.Instance.Logging!);
        collection.AddSingleton(AppConfigs.Instance!);
        collection.AddSingleton(SharedModule.Instance!);

        // scope(s)
        // ...

        // window(s)
        collection.AddWindow<MainWindow>();

        Provider = collection.BuildServiceProvider();
    }

    // ----------------------------------------------------------------------

    /// <summary>
    /// Output log for it's information.
    /// </summary>
    public void Print()
    {
        // nothings to do, for now
    }
}
