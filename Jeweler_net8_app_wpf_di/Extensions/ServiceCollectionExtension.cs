﻿using System;
using System.Windows;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Jeweler_net8_app_wpf_di;

namespace Jeweler_net8_app_wpf_di.Extensions;

public static class ServiceCollectionExtension
{
    public static IServiceCollection AddWindow<T>(this IServiceCollection serviceCollection) where T : Window
    {
        serviceCollection.AddScoped(typeof(ILogger<T>), provider => AppBegin.Instance.Logging!.CreateLogger<T>());
        serviceCollection.AddTransient(typeof(T));
        return serviceCollection;
    }
}
