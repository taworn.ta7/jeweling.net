﻿using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;
using Microsoft.Extensions.Logging;

namespace Jeweler_net8_app_wpf_di;

/// <summary>
/// Interaction logic for MainWindow.xaml
/// </summary>
public partial class MainWindow : Window
{
    private readonly ILogger logger;
    private readonly AppConfigs config;

    public MainWindow(
        ILogger<MainWindow> _logger,
        AppConfigs _config)
    {
        InitializeComponent();
        logger = _logger;
        config = _config;

        Loaded += (sender, e) =>
        {
            logger.LogTrace("I'm tracing.");
            logger.LogDebug("I'm debugging.");
            logger.LogInformation("I'm informing.");
            logger.LogWarning("I'm warning.");
            logger.LogError("I'm error!");
            logger.LogCritical("I'm critical!!");

            using (logger.BeginScope("Inner"))
            {
                logger.LogDebug("Dummy: {Dummy}", config.Dummy);
            }
        };

        Unloaded += (sender, e) =>
        {
        };

        Closed += (sender, e) =>
        {
        };
    }

    // ----------------------------------------------------------------------

}
