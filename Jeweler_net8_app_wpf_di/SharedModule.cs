﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Extensions.Logging;

namespace Jeweler_net8_app_wpf_di;

/// <summary>
/// Shared components in global scope.
/// </summary>
public class SharedModule
{
    private static readonly ILogger logger = AppBegin.Instance.Logging!.CreateLogger<SharedModule>();

    //
    // Components
    //

    public Object Dummy { get; private set; } = null!;

    public static SharedModule Instance
    {
        get => instance ??= new SharedModule();
    }
    private static SharedModule instance = null!;

    private SharedModule()
    {
        Dummy = new Object();
    }

    // ----------------------------------------------------------------------

    /// <summary>
    /// Output log for it's information.
    /// </summary>
    public void Print()
    {
        logger?.LogInformation("""
Shared Module:

* Dummy: {Dummy}

""", Dummy);
    }
}
