﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Jeweler_net8_webapi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AppController : ControllerBase
    {
        private readonly ILogger logger;

        public AppController(
            ILogger<AppController> _logger)
        {
            logger = _logger;
        }

        // ----------------------------------------------------------------------

        [HttpGet(Name = "about")]
        //[Route("about")]
        public IDictionary<string, dynamic> About()
        {
            logger.LogTrace("Trace...");
            logger.LogDebug("Debug...");
            logger.LogInformation("Information...");
            logger.LogWarning("Warning...");
            logger.LogError("Error...");

            var dict = new Dictionary<string, dynamic>();
            dict.Add("app", "Jeweler_net8_webapi");
            dict.Add("version", "0.1.0");
            return dict;
        }
    }
}
