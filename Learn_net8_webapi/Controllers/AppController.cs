﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Learn_net8_webapi.Controllers
{
    [ApiController]
    [Route("api")]
    public class AppController : ControllerBase
    {
        private readonly ILogger logger;

        public AppController(
            ILogger<AppController> _logger)
        {
            logger = _logger;
            logger.LogWarning("Hello, world :)");
        }

        // ----------------------------------------------------------------------

        [HttpGet("about")]
        //[Route("about")]
        public IDictionary<string, dynamic> About()
        {
            var dict = new Dictionary<string, dynamic>();
            dict.Add("app", "Learn_net8_webapi");
            dict.Add("version", "0.1.0");
            return dict;
        }


        [HttpGet("config")]
        //[Route("config")]
        public IDictionary<string, dynamic> Config()
        {
            var dict = new Dictionary<string, dynamic>();
            dict.Add("-", "-");
            return dict;
        }
    }
}
